const express = require("express");
const { remove, sortData, paginate } = require("./controller/post.js");
const cors = require("cors");
const app = express();

app.use(express.json());
app.use(
  cors({
    credentials: true,
    origin: true,
  }),
);

app.get("/api/foo", (req, res) => {
  return res.status(200).json({ success: true });
});
app.get(
  "/api/:database/sort/:actcategory/:column/:sortDirection/:from/:to",
  sortData,
);
app.get("/api/:database/paginate/:actcategory/:from/:to", paginate);
app.delete("/api/:database/:actcategory/remove/:id", remove);

module.exports = app;
