const { baza } = require("../model/baza1.js");
const increase = async (req, res) => {
  for (let i = baza["comments"]["new"].length; i < 11530000; i++) {
    baza["comments"]["new"].push({
      postId: 1,
      id: baza["comments"]["new"].length + i,
      name: "id labore ex et quam laborum",
      email: "Eliseo@gardner.biz",
      body: "laudantium enim quasi est quidem magnam voluptate ipsam eos\ntempora quo necessitatibus\ndolor quam autem quasi\nreiciendis et nam sapiente accusantium",
    });
  }
};
const load = async (req, res) => {
  const { database } = req.params;
  try {
    res.status(200).json(baza[database]);
  } catch (err) {
    res.status(404).json({ message: err.message });
  }
};

const sortData = (req, res) => {
  const { database, actcategory, column, sortDirection, from, to } = req.params;
  try {
    sortDirection === "DESC"
      ? baza &&
      baza[database][actcategory].sort(function(a, b) {
        return typeof a[column] === "string"
          ? a[column].localeCompare(b[column])
          : a[column] - b[column];
      })
      : baza &&
      baza[database][actcategory].sort(function(a, b) {
        return typeof a[column] === "string"
          ? b[column].localeCompare(a[column])
          : b[column] - a[column];
      });
    let ss = baza[database][actcategory].filter((t, i) => {
      return i >= from && i < to && t;
    });

    res.status(200).json({ [actcategory]: ss });
  } catch (err) {
    res.status(404).json({ message: err.message });
  }
};
const paginate = async (req, res) => {
  const { database, actcategory, from, to } = req.params;
  console.log("llllllllllllllllllllllllllll");
  try {
    let ss = baza[database][actcategory].filter((t, i) => {
      return i >= from && i < to && t;
    });

    res.status(200).json({
      len: baza[database][actcategory].length,

      [actcategory]: ss,
    });
  } catch (err) {
    res.status(404).json({ message: err.message });
  }
};

const remove = async (req, res) => {
  const { database, actcategory, id } = req.params;
  try {
    console.log("LEN BEFORE: ", baza[database][actcategory].length);
    const y = baza[database][actcategory].filter((t) => {
      return Number(t.id) !== Number(id) && t;
    });
    console.log("LEN AFTER: ", y.length);
    baza[database][actcategory] = y;
    res.status(200).json({ [actcategory]: y });
  } catch (err) {
    res.status(404).json({ message: err.message });
  }
};

module.exports = {
  remove,
  paginate,
  sortData,
  load,
  increase
}
